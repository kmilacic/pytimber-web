var plot = document.getElementById('chartContainer');
// browser timezone
var tz = document.getElementById('timezone').value;

var chartJSRendered = false;
var canvasJSRendered = false;
var highchartsJSRendered = false;

var data = JSON.parse(plot.dataset.points);
var vars = JSON.parse(plot.dataset.vars.replace(/'/g, '"'));
var units = JSON.parse(plot.dataset.units.replace(/'/g, '"'));
var types = JSON.parse(plot.dataset.types.replace(/'/g, '"'));

var vars_units = vars.map(function(variable, i) {
  return (units[i] !== " ") ? [variable + " [" + units[i] + "]"] : [variable]
});

// total number of points retrieved
function getSum(total, num) {
  return total + Math.round(num);
}
let totalPoints = vars.map((e,z) => data[e][0].length).reduce(getSum,0);

var set = new Set(units);
set = Array.from(set);

// add multiple y axes for different units
function y_axes(){
  a = false;
  tmp_units = []
  tmp_axes = []
  for(i=0; i<vars.length; i++){
    if(!tmp_units.includes(units[i])){
      tmp_units.push(units[i]);
      tmp_axes.push({
        title: {
          text: units.map((e, z) => e === units[i] ? vars_units[z] : '').filter(e => e!= '').join(', '),
          fontsize:16
        },
        labels: {
          fontSize: 12
        },
        opposite: a,
        lineWidth: 1
      });
      a = !a;
    }
  }

  return tmp_axes;
}

let renderHighChartJS = function(){
    if(highchartsJSRendered){
        highchartsJSRendered = false;
        document.getElementById("chartContainer").style.display = "none";
    } else {

        let chartData = vars.map(function(variable, i) {
        
          let points = data[variable][0].map(function(e, j) {
            let datetime = e * 1000;            
            return {
              x: datetime,
              y: data[variable][1][j]
                }
          });

          return {
            name: variable,
            data: points,
            yAxis: set.indexOf(units[i])
          }
        });

        highchartsJSRendered = true;
        document.getElementById("chartContainer").style.display = "block";

        var myChart = new Highcharts.chart('chartContainer', {               
            title: {
                fontSize: 20,
                text: "Plot of " + vars.join(", ") + " over Datetime"
            },

            time: {
                timezone: tz
            },

            yAxis: y_axes(),
    
            xAxis: {
                type: 'datetime',

                title: {
                    text: 'Datetime',
                    fontSize: 16
                },
                labels: {
                    fontSize: 12,
                    format: '{value:%d-%m-%Y}'
                }
            },
    
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
            },
        
            plotOptions: {
                series: {
                    turboThreshold: 0,
                    boostThreshold: 1
                },

                line: {
                    dataLabels: {
                        enabled: false
                    },
		                marker: {
                      enabled: false,
                      symbol: 'circle',
                      radius: 4,
                      states: {
                          hover: {
                              enabled: true,
                              opacity: 0.5,
                              fillColor: 'grey'
                          }
                      }
                  }
                }
            },

            tooltip: {
              valueDecimals: 2,
              followPointer: true,
              shared: true
            },
            
            exporting: {
              sourceHeight:800,
              sourceWidth: 2000,
              scale: 1,
              filename: 'chart'
            },

            series: chartData,
            
            chart: {
                zoomType: 'xy'
            }
    
    });
}}

