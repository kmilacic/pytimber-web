from django.apps import AppConfig


class PytimberConfig(AppConfig):
    name = 'pytimber'
